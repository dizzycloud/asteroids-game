﻿
/* 
class Ball
params: Ball radius, Ball HTML element
*/
function Ball(radius, dom) {
    //window height
    this.containerHeight = 0;
    //window width
    this.containerWidth = 0;
    //ball center horizontal value
    this.x = 0;
    //ball center vertical value
    this.y = 0;
    //ball radius
    this.radius = radius;
    //ball speed up
    this.upSpeed = 5;
    //ball speed down
    this.fallSpeed = 10;
    //bottom boundary
    this.bottom = 0;
    //top boundary
    this.top = 0;
    //left boundary
    this.left = 0;
    //HTML element
    this.dom = dom;
    this.getContainerDimensions();
    this.setInitalDimensions();
    this.draw();
}

//ball movement down
Ball.prototype.fall = function () {
    this.y = this.y + this.fallSpeed;
    this.setTopAndBottom();
    this.draw();
    if (this.bottom > ($("#game").position().top + $("#game").height())) {
        this.gameOver();
    }
};

//ball movement up
Ball.prototype.up = function () {    
    this.y = this.y - this.upSpeed;
    this.setTopAndBottom();
    this.draw();
    if (this.top < ($("#game").position().top)) {
        this.gameOver();
    }
};

//set x ,y, top, bottom and left initial values
Ball.prototype.setInitalDimensions = function () {
    this.x = (this.containerWidth / 2) - this.radius;
    this.y = $("#toplava").height() + ($("#game").height())*2/3
    this.left = this.x - this.radius;
    this.setTopAndBottom();
};

//set top and bottom values
Ball.prototype.setTopAndBottom = function () {
    this.top = this.y - this.radius;
    this.bottom = this.y + this.radius;
};

//set window height and width
Ball.prototype.getContainerDimensions = function () {
    this.containerHeight = $("body").height();
    this.containerWidth = $("body").width();
};

//draw HTML Ball element
Ball.prototype.draw = function () {
    this.dom.css("left",this.left + "px");
    this.dom.css("top",this.top + "px");
};


//GAME OVER
Ball.prototype.gameOver = function () {
    $(".audio").get(0).play();
    alert("GAME OVER");
    location.reload();
};




/* 
class Asteroid
*/
function Asteroid() {
    this.width = 0;
    this.height = 0;
    //right boundary
    this.right = 0;
    //left boundary
    this.left = 0;
    //bottom boundary
    this.bottom = 0;
    //top boundary
    this.top = 0;
    //horizontal movement speed
    this.speed = 20;
    this.dom;
    this.initialDrawAndSetDimensions();
}

//draw HTML Asteroid element and set top and bottom values
Asteroid.prototype.initialDrawAndSetDimensions = function () {
    this.dom = $("<div class='divAsteroid'></div>");
    $("body").append(this.dom);   
    this.height = this.dom.height();
    this.width = this.dom.width();
    this.right = this.width;
    this.bottom = Math.floor(Math.random() * ($("body").height()));
    this.top = this.bottom - this.height;
    this.dom.css("top", this.top + "px");
    this.dom.css("left", this.left + "px");
};

//set new dimensions and draw HTML Asteroid element
//param: Ball instance
Asteroid.prototype.moveAndDraw = function (ball) {
    if (this.right + this.speed * 2 > $("body").width()) {
        // the Asteroid HTML element is going to exceed the container boundaries
        this.dom.remove();
        return;
    }
    this.right = this.right + this.speed;
    this.left = this.right - this.width;
    this.dom.css("left", this.left + "px");
    if (this.checkIfInBall(ball)) {
        ball.gameOver();
    }
};

//check if Asteroid touches the ball instance
//param: Ball instance
//returns: true- Asteroid touches the ball, false- Asteroid doesn't touch the ball
Asteroid.prototype.checkIfInBall = function (ball) {
    if (Math.sqrt(Math.pow(this.right - ball.x, 2) + Math.pow(this.top - ball.y, 2)) < ball.radius ||
       Math.sqrt(Math.pow(this.right - ball.x, 2) + Math.pow(this.bottom - ball.y, 2)) < ball.radius ||
       (this.right > ball.left && this.top < ball.top && this.bottom > ball.bottom)) {
          return true;
    }
    return false;
};

/*
class Game
*/
function Game(ball) {
    this.setBtn(ball);
    this.getTweet();
}
//get Obama's last tweet
Game.prototype.getTweet = function () {
    var username = 'BarackObama';
    $.getJSON(
        "http://search.twitter.com/search.json?q=from:" + username + "&rpp=1&callback=?",
         function (data) {
             var lastTweet = data.results[0].text;
             $("#obama").append("<div>" + lastTweet + "</div>");
         }
    );
};

//set HTML start button positon 
Game.prototype.setBtn = function (ball) {
    var btn = $(".divBtn");
    var btnLeft = ball.x - btn.width() / 2;
    btn.css("left", btnLeft + "px");
};


$(document).ready(function () {
    $("#BtnUp").hide();
    var earth = new Ball(($("#ball").width()) / 2, $("#ball"));
    var game = new Game(earth);

    $("#BtnUp").click(function () {
        earth.up();
    });

    $("#BtnStart").click(function () {
        $("#BtnStart").hide();
        $("#instructions").fadeOut();
        $("#BtnUp").show();
        var allAstroids = new Array();
        allAstroids.push(new Asteroid());
        setInterval(function () {
            allAstroids.push(new Asteroid());
        }, 5000);
        setInterval(function () {
            earth.fall();
        }, 1000);
        setInterval(function () {
            for (var i = 0; i < allAstroids.length; i++) {
                var ast = allAstroids[i];
                ast.moveAndDraw(earth);
            }
        }, 500);
    });
});

window.jsonFlickrApi = function (x) {
    debugger
}
